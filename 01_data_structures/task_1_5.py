# -*- coding: utf-8 -*-
'''
Задание 1.5

Из строк pages1 и pages2 получить список страниц,
которые есть и в pages1  и в  pages2.

Результатом должен быть список: ['1', '3', '8']

Ограничение: Все задания надо выполнять используя только пройденные темы.

'''

pages1 = 'интересные страницы: 1,2,3,5,8'
pages2 = 'интересные страницы: 1,3,8,9'
pages= []

for i in pages1 :
    if i in pages:
        continue
    for j in pages2:
        if i == j:
            pages.append(i)
            break

print(pages[11:])